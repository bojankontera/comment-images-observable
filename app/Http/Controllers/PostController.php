<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\SubmitPostEvent;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{


    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        // STORE POST
        $post = Post::create([
            'title' => $request['title'],
            'body' => $request['body'],
        ]);

        // STORE COMMENTS AND IMAGES THROUGH EVENT LISTENER
        event(new SubmitPostEvent($post, $request));

        return view('posts.show', [
            'post' => Post::findOrFail($post->id),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('posts.show', [
            'post' => Post::findOrFail($id),
        ]);
    }

}
