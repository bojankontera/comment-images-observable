<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubmitPostEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $post;
    public $request;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($post, $request)
    {
        $this->post = $post;
        $this->request = $request;
    }
}
