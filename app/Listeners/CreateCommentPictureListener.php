<?php

namespace App\Listeners;

use App\Comment;
use App\Events\SubmitPostEvent;
use Illuminate\Http\Request;

class CreateCommentPictureListener
{
    /**
     * Handle the event.
     *
     * @param  SubmitPostEvent $event
     * @param Request $request
     * @return void
     */
    public function handle(SubmitPostEvent $event)
    {

        // for every comment
        foreach($event->request->comments as $key => $value) {

            // comment store
            $comment = Comment::create([
                'post_id' => $event->post->id,
                'title' => $value['title'],
                'body' => $value['body'],
            ]);

            // add media to comment
            $comment->addMedia($event->request->image[$key])->toMediaCollection('comment');
        }
    }
}
