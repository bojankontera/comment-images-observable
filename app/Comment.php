<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Comment extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'post_id',
        'title',
        'body',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
