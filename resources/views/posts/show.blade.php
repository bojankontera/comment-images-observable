@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Post Details</div>
                    <div class="card-body">
                        Post Name:  {{$post->title}} <br/><br/>
                        Post Description : {{$post->body}} <br/><br/>
                        Post comment:
                        <div class="row">
                            @foreach($post->comments as $comment)
                                <div class="col-md-6">
                                    <div class="card-body">
                                        <h3>{{$comment->title}}</h3>
                                        <hr>
                                        <h4>{{$comment->body}}</h4>
                                        <img src="{{$comment->getFirstMediaUrl('comment')}}" height="100px"/>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
