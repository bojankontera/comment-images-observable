@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Post</div>

                    <div class="card-body">
                        <form method="POST" action="{{route('posts.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Post title</label>
                                <input name="title" type="text" class="form-control" id="title">
                            </div>
                            <div class="form-group">
                                <label>Post body</label>
                                <textarea name="body" class="form-control" id="body"></textarea>
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Comment title</label>
                                                <input type="text" name="comments[0][title]" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <textarea name="comments[0][body]" id="" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="FormControlFile">Post Image</label>
                                            <input type="file" name="image[]" id="image" class="form-control-file">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Comment title</label>
                                                <input type="text" name="comments[1][title]"  class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <textarea name="comments[1][body]" id="" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="FormControlFile">Post Image</label>
                                            <input type="file" name="image[]" id="image" class="form-control-file">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Submit"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
